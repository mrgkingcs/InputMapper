# InputMapper

A simple, janky python tool for mapping input controls to a virtual device in Linux.

## Requirements

- Python3
- libevdev for python (pip3 install libevdev)
- permissions tinkering

## Permissions

By default, your user probably won't have access permissions to the input devices.

### Step 1
Add yourself to the 'input' group. e.g. 
`sudo usermod -a -G input <yourusername>`

I found [this page](https://linuxize.com/post/how-to-add-user-to-group-in-linux/) useful.

### Step 2
Change /dev/uinput permissions so you can create the virtual device without being root.

Create the file `/etc/udev/rules.d/99-uinput.rules` and put the following in its contents:
`KERNEL=="uinput", GROUP="input", MODE="0660"`

### Step 3
Reboot the system.

(You only need to logout and back in for the group change, but I believe the /dev/uinput permissions need a full reboot)

## Usage

### Gathering input IDs/ranges
Run 'logger.py' to bring up a window with a tab for each device.
Press the desired button(s) on the input device and a summary of them should appear in the tab.
For axes, move the axis through its full range to get the correct min/max values.

### Writing a config file

See testConfig for an example.

#### General format

Each device needs a device line (copy/paste from logger gui)

`DEVICE Microsoft MicrosoftÂ® 2.4GHz Transceiver v9.0 Mouse`

The device line is then followed by mappings from that device to the virtual device.
e.g.

`BTN_LEFT:272 -> BTN_2`

`BTN_RIGHT:273=(0,1) -> BTN_0=(1,0)`

#### Finding valid button/axis IDs

If you have the linux sources installed, you can grep input-event-codes.h for them.
e.g. under Manjaro:

`grep ABS /usr/include/linux/input-event-codes.h`

`grep KEY /usr/include/linux/input-event-codes.h`


#### Mapping a button
`BTN_LEFT:272 -> BTN_2`

This maps the left mouse button to button 2 on the virtual device

#### Inverting a button
`BTN_TRIGGER_HAPPY14:717=(0,1) -> BTN_TRIGGER_HAPPY14=(1,0)`

This will make the virtual device button pressed when the input button is released (and vice versa)

#### Generating multiple output events from a single input event

`BTN_MIDDLE:274=0 -> BTN_1=1`

`BTN_MIDDLE:274=0 -> BTN_1=0`

Both these mappings will match the middle mouse button being released (value going to 0).
The first will set button 1 to 1 (pressed), and the second will immediately set it back to 0 (released).  No delay is implemented as yet, so this might not be very reliable (the change to pressed and back might be too quick for the game to pick up)

#### mapping an axis
`ABS_X:0 -> ABS_X`

This is a straight mapping of an input axis to a virtual device axis.  The input values will be passed in untouched.

#### remapped axis values
`ABS_X:0=(0,65535) -> ABS_Z=(1024, 2048)`

This will map input values from the range 0-65535 into the range 1024-2048 for the virtual device.  This could be useful if the game has a single axis (e.g. accelerate/brake) that you want to control with two separate axes (e.g. pedals)

#### remapped inverted axis
`ABS_Y:1=(0,65535) -> ABS_Y=(65535,0)`

This will invert the input values from the input device for the virtual device.  e.g. an input value of 0 will be 65535 on the virtual device.

#### partial axis to remapped axis
`ABS_Y:1=(0,32767) -> ABS_RY=(0,65535)`

`ABS_Y:1=(32768,65535) -> ABS_RZ=(0,65535)`

This will map some of the input axis (the lower half) to a full output axis.  This could be useful if you want a single axis (e.g. y axis) to control two separate axes (e.g. accelerate and brake)
 
### Running the device

If your python3 is /usr/bin/python3, you can just run the following from the command line:

`runDevice.py myConfigFile`


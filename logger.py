#!/usr/bin/python3
import tkinter as tk
from tkinter import ttk
from tkinter.scrolledtext import ScrolledText

from controller import *

### config consts
DEFAULT_FONT = ('monospace', '20', 'bold')

### class to turn input events into a tidy summary
class ControllerSummary:
    def __init__(self, controller):
        self.__controller = controller
        self.__ranges = {}
        self.dirty = True
    
    def getSummary(self):
        self.dirty = False
        summary = self.__controller.getName() + '\n'
        
        for eventCodeName, valueRange in self.__ranges.items():
            summary += "{}=({},{})\n".format(eventCodeName, valueRange[0], valueRange[1])
        
        return summary

    def processEvent(self, event):
        eventCodeName = str(event.code)
        if eventCodeName[0:3] != "SYN":
            if eventCodeName not in self.__ranges:
                self.__ranges[eventCodeName] = [event.value, event.value]
                self.dirty = True
            elif event.value < self.__ranges[eventCodeName][0]:
                self.__ranges[eventCodeName][0] = event.value
                self.dirty = True
            elif event.value > self.__ranges[eventCodeName][1]:
                self.__ranges[eventCodeName][1] = event.value
                self.dirty = True


# class to handle the main window
class MainFormController:
    def __init__(self):
        self.__logMap = {}
        self.__controllerList = []
        self.__controllerSummaryList = {}

        self.__mainWin = tk.Tk()

        self.__style = ttk.Style()
        self.__style.configure('TNotebook.Tab', font=DEFAULT_FONT)

        self.__notebook = ttk.Notebook(self.__mainWin)
        self.__notebook.pack(fill=tk.BOTH, expand=tk.TRUE)
        self.__createTab("log")
        
        self.__createDeviceTabs()

    def __createTab(self, name):
        frame = ttk.Frame(self.__notebook)
        frame.pack(fill=tk.BOTH, expand=tk.TRUE)
        self.__notebook.add(frame, text=name)

        scrolledText = ScrolledText(frame)
        scrolledText.config(state='disabled', font=DEFAULT_FONT)
        scrolledText.pack(fill=tk.BOTH, expand=tk.TRUE)
        self.__logMap[name] = scrolledText

    def __createDeviceTabs(self):
        devicePathList = getDevicePaths()
        self.__controllerList = makeControllerList(devicePathList)

        for controller in self.__controllerList:
            self.__log("Found device {}".format(controller.getName()))
            self.__controllerSummaryList[controller] = ControllerSummary(controller)
            self.__createTab(controller.getName())

    def __clearLog(self, tabName='log'):
        if tabName in self.__logMap:
            logWidget = self.__logMap[tabName]
            logWidget.config(state='normal')
            logWidget.delete('1.0', tk.END)
            logWidget.config(state='disabled')

    def __log(self, message, tabName='log'):
        if tabName in self.__logMap:
            logWidget = self.__logMap[tabName]
            logWidget.config(state='normal')
            logWidget.insert(tk.END, str(message)+'\n')
            logWidget.config(state='disabled')

    def __processEvents(self):
        for controller in self.__controllerList:
            summary = self.__controllerSummaryList[controller]
            try:
                for event in controller.getEvents():
                    summary.processEvent(event)
            except libevdev.EventsDroppedException:
                for event in controller.syncEvent():
                    summary.processEvent(event)
            
            if summary.dirty:
                self.__clearLog(controller.getName())
                self.__log(summary.getSummary(), controller.getName())

        self.__mainWin.after(250, lambda : self.__processEvents())

    def run(self):
        self.__mainWin.after(250, lambda : self.__processEvents())
        self.__mainWin.mainloop()


if __name__ == "__main__":
    mainController = MainFormController()
    mainController.run()
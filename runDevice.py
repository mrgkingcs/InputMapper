#!/usr/bin/python3
import libevdev
from libevdev import InputEvent
import sys
import time

from controller import *

## config defs
LOOP_DELAY = 0.05

## global vars
inputControllers = {}


### for testing
testConfigData = \
"""
# first device
DEVICE Microsoft MicrosoftÂ® 2.4GHz Transceiver v9.0 Mouse
BTN_LEFT:272 -> BTN_THUMB
BTN_RIGHT:273=(0,1) -> BTN_0=(1,0)

# pulse output button on single input button trigger
BTN_MIDDLE:274=0 -> BTN_1=1
BTN_MIDDLE:274=0 -> BTN_1=0

# second device
DEVICE Thustmaster Joystick - HOTAS Warthog
BTN_THUMB:289 -> BTN_1

# direct axis, no mapping
ABS_X:0 -> ABS_X

# remapped axis
ABS_X:0=(0,65535) -> ABS_Z=(1024, 2048)

# remapped inverted axis
ABS_Y:1=(0,65535) -> ABS_Y=(65535,0)

# partial axis to remapped axis
ABS_Y:1=(0,32767) -> ABS_RY=(0,65535)

# test for constant button trigger
DEVICE Thrustmaster Throttle - HOTAS Warthog
BTN_TRIGGER_HAPPY14:717=(0,1) -> BTN_TRIGGER_HAPPY14=(1,0)
"""

### class to handle the mapping of an event
class EventMapping:
    BtnEventCodeMap = libevdev.EV_KEY.__dict__
    AbsEventCodeMap = libevdev.EV_ABS.__dict__

    def __init__(self, inputCodeStr, outputCodeStr):
        self.__inputCodeStr = inputCodeStr
        self.__inputLow = None
        self.__inputHigh = None
        self.__outputCodeStr = outputCodeStr
        self.__outputLow = None
        self.__outputHigh = None
        self.__prevInputValue = None
    
    def getInputCodeStr(self):
        inputCodeStr = self.__inputCodeStr
        colonIndex = inputCodeStr.find(':')
        if colonIndex != -1:
            inputCodeStr = inputCodeStr[:colonIndex]
        return inputCodeStr

    def getInputCode(self):
        inputCode = None
        inputCodeStr = self.getInputCodeStr()
        category = inputCodeStr[0:3]
        if category == "BTN":
            inputCode = EventMapping.BtnEventCodeMap[inputCodeStr]
        else:
            inputCode = EventMapping.AbsEventCodeMap[inputCodeStr]
        return inputCode

    def getOutputCodeStr(self):
        outputCodeStr = self.__outputCodeStr
        colonIndex = outputCodeStr.find(':')
        if colonIndex != -1:
            outputCodeStr = outputCodeStr[:colonIndex]
        return outputCodeStr

    def getOutputCode(self):
        outputCode = None
        outputCodeStr = self.getOutputCodeStr()
        category = outputCodeStr[0:3]
        if category == "BTN":
            outputCode = EventMapping.BtnEventCodeMap[outputCodeStr]
        else:
            outputCode = EventMapping.AbsEventCodeMap[outputCodeStr]
        return outputCode

    def getOutputData(self):
        data = None
        if self.__outputCodeStr[0:3] == 'ABS':
            low = 0
            high = 65535
            if self.__outputLow is not None:
                low = min(self.__outputLow, self.__outputHigh)
                high = max(self.__outputLow, self.__outputHigh)
            elif self.__inputLow is not None:
                low = min(self.__inputLow, self.__inputHigh)
                high = max(self.__inputLow, self.__inputHigh)                
            data = libevdev.InputAbsInfo(minimum=low, maximum=high)
        return data

    def setInputRange(self, low, high):
        self.__inputLow = low
        self.__inputHigh = high

    def setOutputRange(self, low, high):
        self.__outputLow = low
        self.__outputHigh = high

    def __processTrigger(self, inputValue):
        value = None
        if self.__inputLow is not None:
            if inputValue == self.__inputLow:
                value = self.__outputLow
        return value
    
    def __processRange(self, inputValue):
        fraction = None
        if self.__inputLow is not None:
            if inputValue <= self.__inputLow:
                fraction = 0
            elif self.__inputHigh is not None:
                if inputValue >= self.__inputHigh:
                    fraction = 1
                else:
                    fraction = (inputValue - self.__inputLow) / (self.__inputHigh - self.__inputLow)
            ## invalid for __inputHigh to be None when __inputLow is not
        ## ...and vice versa (so assume __inputHigh is None)

        if fraction is None:
            outputValue = inputValue
        else:
            outputValue = int(self.__outputLow + fraction*(self.__outputHigh - self.__outputLow)) 

        return outputValue

    def isTriggered(self, inputValue):
        # early cut-out if input value hasn't changed from last time
        if inputValue == self.__prevInputValue:
            return False
        else:
            self.__prevInputValue = inputValue

        if self.__inputLow is not None:     # not a value-passthrough
            if self.__inputLow == self.__inputHigh:     # a triggered input
                if inputValue != self.__inputLow:       # input value doesn't match
                    return False

        return True

    def getOutputValue(self, inputValue):
        outputValue = None
        fraction = None
        if self.__inputLow is not None:
            if self.__inputLow == self.__inputHigh:
                outputValue = self.__processTrigger(inputValue)
            else:
                outputValue = self.__processRange(inputValue)

        if outputValue is None:
            outputValue = inputValue

        #self.__prevInputValue = inputValue

        return outputValue            

### parse an individual code string (e.g. "BTN_RIGHT:273=(0,1)")
def parseCodeStr(codeStr):
    eqIndex = codeStr.find("=")
    if eqIndex != -1:
        value = codeStr[eqIndex+1:].strip()
        codeStr = codeStr[:eqIndex].strip()
        if value[0] == '(':
            value = value[1:-1]
            commaIndex = value.find(',')
            if commaIndex != -1:
                low = int(value[:commaIndex])
                high = int(value[commaIndex+1:])
                value = (low,high)
            else:
                value = (value, value)
        else:
            value = (int(value), int(value))
    else:
        value = None 
    
    return codeStr, value

### main fn to parse config text
def parseConfig(configText):
    config = {}
    lines = configText.split('\n')
    lineNumber = 1
    for line in lines:
        if len(line.strip()) == 0 or line.startswith('#'):
            pass
        elif line.startswith("DEVICE"):
            deviceName = line[6:].strip()
            if deviceName not in config.keys():
                config[deviceName] = {}
        else:
            arrowIndex = line.find("->")
            if arrowIndex != -1:
                inputCodeStr, inputValue = parseCodeStr(line[:arrowIndex].strip())
                outputCodeStr, outputValue = parseCodeStr(line[arrowIndex+2:].strip())

                mapping = EventMapping(inputCodeStr, outputCodeStr)
                if inputValue is not None:
                    mapping.setInputRange(inputValue[0], inputValue[1])
                if outputValue is not None:
                    mapping.setOutputRange(outputValue[0], outputValue[1])

                if deviceName not in config:
                    config[deviceName] = {}
                
                if inputCodeStr not in config[deviceName]:
                    config[deviceName][inputCodeStr] = []

                config[deviceName][inputCodeStr].append(mapping)


        lineNumber+=1
    
    return config


def mapSingleEvent(controller, event):
    result = []
    deviceConfig = inputControllers[controller]
    eventCodeStr = str(event.code)

    if eventCodeStr in deviceConfig.keys():
        eventMappingList = deviceConfig[eventCodeStr]

        for eventMapping in eventMappingList:
            if eventMapping.isTriggered(event.value):
                mappedCode = eventMapping.getOutputCode()
                mappedValue = eventMapping.getOutputValue(event.value)

                print("{}={} -> {}={}".format(event.code,event.value, mappedCode, mappedValue))
                result.append(InputEvent(mappedCode, mappedValue))
                result.append(InputEvent(libevdev.EV_SYN.SYN_REPORT, 0))

    return result


def mapEvents(controller):
    mappedEvents = []

    try:
        for event in controller.getEvents():
            mappedEvents += mapSingleEvent(controller, event)
    except libevdev.EventsDroppedException:
        for event in controller.syncEvents():
            mappedEvents += mapSingleEvent(controller, event)
    
    return mappedEvents


def main(configData):
    config = parseConfig(configData)

    devicePathList = getDevicePaths()
    controllers = makeControllerList(devicePathList)

    for deviceName in config.keys():
        for controller in controllers:
            if controller.getName() == deviceName:
                inputControllers[controller] = config[deviceName]


    dev = libevdev.Device()
    dev.name = "Virtual Controller"
    for controller in inputControllers.keys():
        controllerConfig = config[controller.getName()]
        for inputKey, mappingList in controllerConfig.items():
            for mapping in mappingList:
                eventCode = mapping.getOutputCode()

                eventData = mapping.getOutputData() 

                print("Enabling event {}".format(eventCode))
                dev.enable(eventCode, data=eventData)

    virtDev = dev.create_uinput_device()

    try:
        while True:
            eventsToSend = []
            for controller in inputControllers:
                eventsToSend += mapEvents(controller)        

            if len(eventsToSend) > 0:
                virtDev.send_events(eventsToSend)

            time.sleep(LOOP_DELAY)

    except KeyboardInterrupt:
        pass

if __name__ == "__main__":
    configData = testConfigData
    if len(sys.argv) > 1:
        try:
            filename = sys.argv[1]
            with open(filename, "r") as configFile:
                configData = configFile.read()
        except (IOError, OSError, FileNotFoundError, PermissionError):
            print("Unable to read config file {}".format(filename))


    main(configData)
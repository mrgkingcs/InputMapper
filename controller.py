import os
import libevdev

## config consts
DEVICE_DIR = '/dev/input'

def getDevicePaths():
    devicePaths = []
    
    for filename in os.listdir(DEVICE_DIR):
        devicePaths.append(os.path.join(DEVICE_DIR, filename))
    
    return devicePaths
        

def makeControllerList(devicePathList):
    controllerList = []

    for path in devicePathList:
        try:
            controller = Controller(path)
            if controller.has(libevdev.EV_ABS) or controller.has(libevdev.EV_KEY):
                controllerList.append(controller)
        except KeyboardInterrupt:
            pass
        except IOError as e:
            print("Failed to open device {}: {}".format(path, e))

    return controllerList


class Controller:
    def __init__(self, devPath):
        self.__fd = None
        self.__devPath = devPath
        self.__fd = open(devPath, "rb")
        os.set_blocking(self.__fd.fileno(), False)

        self.__dev = libevdev.Device(self.__fd)
    
    def __del__(self):
        self.__dev = None
        if self.__fd is not None:
            self.__fd.close()
    
    def has(self, eventType):
        return self.__dev.has(eventType)

    def getName(self):
        return self.__dev.name
    
    def getEvents(self):
        return self.__dev.events()
    
    def syncEvents(self):
        return self.__dev.sync()


